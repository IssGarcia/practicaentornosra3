package password;

import java.util.Scanner;

public class Password {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				Scanner scanner = new Scanner(System.in);
				mostrarMenu();
				int longitud = scanner.nextInt();
				System.out.println("Elige tipo de password: ");
				int opcion = scanner.nextInt();
				String password = "";
				password = programaSwitch(longitud, opcion, password);

				System.out.println(password);
				scanner.close();
		}

		private static String programaSwitch(int longitud, int opcion, String password) {
			switch (opcion) {
			case 1:
				password = delaAalaZ(longitud, password);
				break;
			case 2:
				password = numeros(longitud, password);
				break;
			case 3:
				password = letrasyNumeros(longitud, password);
				break;
			case 4:
				password = letrasNumerosyCaracteres(longitud, password);
				break;
			}
			return password;
		}

		private static String letrasNumerosyCaracteres(int longitud, String password) {
			for (int i = 0; i < longitud; i++) {
				int n;
				n = (int) (Math.random() * 3);
				if (n == 1) {
					char letra4=' ';
					password = aleatorioyConcatenar(password, letra4, 26,65);
				} else if (n == 2) {
					char caracter4;
					caracter4 = (char) ((Math.random() * 15) + 33);
					password += caracter4;
				} else {
					int numero4;
					numero4 = (int) (Math.random() * 10);
					password += numero4;
				}
			}
			return password;
		}

		private static String letrasyNumeros(int longitud, String password) {
			for (int i = 0; i < longitud; i++) {
				int n;
				n = (int) (Math.random() * 2);
				if (n == 1) {
					char letra3=' ';
					 password = aleatorioyConcatenar(password,letra3,26,65);
				} else {
					char caracter3=' ';
					password = aleatorioyConcatenar(password,caracter3,16,33);
				}
			}
			return password;
		}

		private static String numeros(int longitud, String password) {
			for (int i = 0; i < longitud; i++) {
				int numero2;
				numero2 = (int) (Math.random() * 10);
				password += numero2;
			}
			return password;
		}

		private static String delaAalaZ(int longitud, String password) {
			for (int i = 0; i < longitud; i++) {
				char letra1=' ';
				password = aleatorioyConcatenar(password, letra1, 26,65);
				
				
			}
			return password;
		}

		private static String aleatorioyConcatenar(String password, char caracter, int num1, int num2) {
			
			caracter = (char) ((Math.random() * num1) + num2);
			password += caracter;
			return password;
		}



		private static void mostrarMenu() {
			System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
			System.out.println("1 - Caracteres desde A - Z");
			System.out.println("2 - Numeros del 0 al 9");
			System.out.println("3 - Letras y caracteres especiales");
			System.out.println("4 - Letras, numeros y caracteres especiales");
			System.out.println("Introduce la longitud de la cadena: ");

	}

}
