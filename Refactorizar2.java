package refactorizar2;

import java.util.Scanner;

		
public class Refactorizar2 {
	
	
	final static int MAXIMOALUMNOS = 10;

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner lector = new Scanner(System.in);
		
		
		int[] arrays = new int[MAXIMOALUMNOS]; //el corchete al lado del la variable
		
		leerNotas(lector, arrays);	
		
		System.out.println("El resultado es: " + recorrerArray(arrays));
		
		lector.close();
		
		
	}
	private static void leerNotas(Scanner lector, int[] arrays) {
		for( int i =0;i<MAXIMOALUMNOS;i++){
			System.out.println("Introduce nota media de alumno");
			arrays[i] = lector.nextInt();
		}
	}
	static double recorrerArray(int vector[])//el corchete al lado de la variable
	{
		double suma = 0;
		for(int a=0;a<MAXIMOALUMNOS;a++) {
			suma=suma+vector[a];
		}
		return suma/10;
	}

}
