package ahorcado;

import java.io.File;
import java.util.Scanner;

public class Ahorcado {


		// TODO Auto-generated method stub
		private final static byte NUM_PALABRAS = 20;
		private final static byte FALLOS = 7;
		private static String[] palabras = new String[NUM_PALABRAS];
		private static String ruta = "C:\\Users\\isabe\\Desktop\\1�DAW\\ENTORNOS DESARROLLO\\TEMA 3-REFACTORIZAR\\PASSWORD_FERNANDO\\master\\RefactorizarAhorcado\\src\\palabras.txt";
		
		public static void main(String[] args) {

			

			cargarFichero();

			Scanner input = new Scanner(System.in);

			String palabraSecreta = crearAleatorio();

			char[][] caracteresPalabra = crearVector(palabraSecreta);

			String caracteresElegidos = "";
			int fallos;
			boolean acertado;
			System.out.println("Acierta la palabra");
			do {

				caracteresElegidos = pedirCaracteres(input, caracteresPalabra, caracteresElegidos);
				fallos = 0;

				
				fallos = caracteresEncontrado(caracteresPalabra, caracteresElegidos, fallos);

				casosFallos(fallos);

				hasPerdido(palabraSecreta, fallos);
				acertado = true;
				acertado = heAcertado(caracteresPalabra, acertado);

			} while (!acertado && fallos < FALLOS);

			input.close();

				
			}

		private static boolean heAcertado(char[][] caracteresPalabra, boolean acertado) {
			for (int i = 0; i < caracteresPalabra[1].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					acertado = false;
					break;
				}
			}
			if (acertado)
				System.out.println("Has Acertado ");
			return acertado;
		}

		private static void hasPerdido(String palabraSecreta, int fallos) {
			if (fallos >= FALLOS) {
				System.out.println("Has perdido: " + palabraSecreta);
			}
		}

		private static int caracteresEncontrado(char[][] caracteresPalabra, String caracteresElegidos, int fallos) {
			boolean encontrado;
			for (int j = 0; j < caracteresElegidos.length(); j++) {
				encontrado = false;
				for (int i = 0; i < caracteresPalabra[0].length; i++) {
					if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
						caracteresPalabra[1][i] = '1';
						encontrado = true;
					}
				}
				if (!encontrado)
					fallos++;
			}
			return fallos;
		}

		private static String pedirCaracteres(Scanner input, char[][] caracteresPalabra, String caracteresElegidos) {
			System.out.println("####################################");

			pintarLineas(caracteresPalabra);
			System.out.println();

			System.out.println("Introduce una letra o acierta la palabra");
			System.out.println("Caracteres Elegidos: " + caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();
			return caracteresElegidos;
		}

		private static void pintarLineas(char[][] caracteresPalabra) {
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					System.out.print(" -");
				} else {
					System.out.print(" " + caracteresPalabra[0][i]);
				}
			}
		}

		private static char[][] crearVector(String palabraSecreta) {
			char[][] caracteresPalabra = new char[2][];
			caracteresPalabra[0] = palabraSecreta.toCharArray();
			caracteresPalabra[1] = new char[caracteresPalabra[0].length];
			return caracteresPalabra;
		}

		private static String crearAleatorio() {
			String palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
			return palabraSecreta;
		}

		private static void casosFallos(int fallos) {
			switch (fallos) {
			case 1:

				fallo1();
				break;
			case 2:

				fallo2();
				break;
			case 3:
				fallo3();
				break;
			case 4:
				fallo4();
				break;
			case 5:
				fallo5();
				break;
			case 6:
				fallo6();
				break;
			case 7:
				fallo7();
				break;
			}
		}

		private static void cargarFichero() {
			File fich = new File(ruta);
			Scanner inputFichero = null;

			try {
				inputFichero = new Scanner(fich);
				for (int i = 0; i < NUM_PALABRAS; i++) {
					palabras[i] = inputFichero.nextLine();
				}
			} catch (Exception e) {
				System.out.println("Error al abrir fichero: " + e.getMessage());
			} finally {
				if (fich != null && inputFichero != null)
					inputFichero.close();
			}
		}

		private static void fallo1() {
			System.out.println("     ___");
		}

		private static void fallo7() {
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
		}

		private static void fallo6() {
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			fallo1();
		}

		private static void fallo5() {
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			fallo1();
		}

		private static void fallo4() {
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			fallo1();
		}

		private static void fallo3() {
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			fallo1();
		}

		private static void fallo2() {
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			fallo1();

	}

}
